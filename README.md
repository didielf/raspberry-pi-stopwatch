# Raspberry Pi Stopwatch

Two stopwatches connected over wifi using two Raspberry PI. There is a 4-7 segment connected to each device to display the timer. The display shows
the time using minutes, seconds and milliseconds (m:ss:ms). One of the Raspberry PI is the server and the other will act as a client. Using python, both 4-7 segments can be controlled from either device.

![Setup](setup initial.png)

![Setup](running example.png)
