import socket
import threading
import RPi.GPIO as GPIO
import time

host = ''
port = 5561
state = 'off'


class convert:
    def stt(self, d):
        ms = int((d - int(d)) * 10)
        s = int(d)
        m = int(s / 60)
        s = s - (m *60)
        sm = "0" + str(m) if m < 10 else str(m)
        ss = "0" + str(s) if s < 10 else str(s)
        return sm[1] + ss + str(ms)

def stopwatch():
    digits = (22,27,17,24)
    segments =  (11,4,23,8,7,10,18,25)
    GPIO.setwarnings(False)

     
    num = {
        '0':(1,1,1,1,1,1,0),
        '1':(0,1,1,0,0,0,0),
        '2':(1,1,0,1,1,0,1),
        '3':(1,1,1,1,0,0,1),
        '4':(0,1,1,0,0,1,1),
        '5':(1,0,1,1,0,1,1),
        '6':(1,0,1,1,1,1,1),
        '7':(1,1,1,0,0,0,0),
        '8':(1,1,1,1,1,1,1),
        '9':(1,1,1,1,0,1,1)}

    GPIO.setmode(GPIO.BCM)
    for segment in segments:
        GPIO.setup(segment, GPIO.OUT)
        GPIO.output(segment, 0)

    for digit in digits:
        GPIO.setup(digit, GPIO.OUT)
        GPIO.output(digit, 1)
    
    reset_starttime = True
    start_time = time.time()
    convert_time = convert()
    global state
    try:
        while True:
            if state == "on":
                if reset_starttime == True:
                    start_time = time.time()
                    reset_starttime = False
                    
                stop_time = time.time();
                elapsed_time = stop_time - start_time
                s = convert_time.stt(elapsed_time)
                for digit in range(4):
                    for loop in range(0,7):
                        GPIO.output(segments[loop],num[s[digit]][loop])
                        if digit == 2 or digit == 0:
                            GPIO.output(25,1)
                        else:
                            GPIO.output(25,0)
                        
                    GPIO.output(digits[digit],0)
                    time.sleep(0.001)
                    GPIO.output(digits[digit],1)
            else:
                reset_starttime = True
                
    except KeyboardInterrupt:
        GPIO.cleanup()

def setupServer():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
    print("Socket created.")
    try:
        s.bind((host, port))
    except socket.error as msg:
        print(msg)
    print("Socket bind complete.")
    return s

def setupConnection():
    s.listen(1) #only one connection at a time.
    conn, address = s.accept()
    print("Connected to: " + address[0] + ":" + str(address[1]))
    return conn




def listen():
    global state
    global conn
    #receive data
    #print("start lisen")
    try:       
        while True:
            data = conn.recv(1024)
            data = data.decode('utf-8')
            dataMessage = data.split(' ', 1)
            command = dataMessage[0]
            if command == "on":
                state = "on"
            elif command == "off":
                state = "off"
            else:
                reply = "Unknown Command"
    except KeyboardInterrupt:
        pass      
    conn.close() 
                  

s = setupServer()
conn = setupConnection()

s_thread = threading.Thread(target=stopwatch, args=())
l_thread = threading.Thread(target=listen, args=())
s_thread.start()
l_thread.start()
while True:
    try:
        command = input("Enter a command: ")
        state = command
        conn.sendall(str.encode(command))
    except KeyboardInterrupt:
        s.close()
        break

